const paths = document.getElementsByTagName('path');
const $influenceInfo = document.getElementById('influence-info'),
      $influenceDesc = document.getElementById('influence-info__desc'),
      $fractionValue = document.getElementById('fraction__value');

for (let i = 0; i < paths.length; i++) {
  paths[i].addEventListener('mousemove', function() {
    $influenceInfo.style.display = 'flex';
    $influenceInfo.style.left = event.pageX + 10 + 'px';
    $influenceInfo.style.top = event.pageY + 10 + 'px';

    console.log(paths[i].getAttribute('stroke-width'));
    let color = paths[i].getAttribute('stroke');
    color = color.substring(5, color.length - 1).split(',');
    color = `${color[0]}, ${color[1]}, ${color[2]}`;
    console.log(color);

    $influenceDesc.innerHTML = `<span style="color: rgb(${color})">${paths[i].getAttribute('source')}</span> влияет на <span style="color: rgb(${color})">${paths[i].getAttribute('target')}</span>:`;
    $fractionValue.innerText = paths[i].getAttribute('stroke-width');
  });

  for (let i = 0; i < paths.length; i++) {
    paths[i].addEventListener('mouseout', function() {
      $influenceInfo.style.display = 'none';
    });
  }

  let temp;

  paths[i].addEventListener('mouseover', function() {
    paths[i].setAttribute(
      'stroke-width',
      parseInt(paths[i].getAttribute('stroke-width')) + 2,
    );
    temp = paths[i].getAttribute('marker-end');
    paths[i].setAttribute('marker-end', '');
  });
  paths[i].addEventListener('mouseout', function() {
    paths[i].setAttribute(
      'stroke-width',
      parseInt(paths[i].getAttribute('stroke-width')) - 2,
    );
    paths[i].setAttribute('marker-end', temp);
  });
}
