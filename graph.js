const xhr = new XMLHttpRequest();
xhr.open('GET', '/preproc/graph.json', true);
xhr.onreadystatechange = function() {
  if (xhr.readyState !== 4) return;
  if (xhr.status === 200) {
    generateGraph(xhr.responseText);
  }
};
xhr.send();

generateGraph = data => {
  const links = JSON.parse(data);
  let colors = [];
  const nodes = {};

  // Compute the distinct nodes from the links.
  links.forEach(function(link) {
    link.source =
      nodes[link.source] || (nodes[link.source] = { name: link.source });
    link.target =
      nodes[link.target] || (nodes[link.target] = { name: link.target });
  });

  console.log(nodes);

  const width = window.innerWidth,
    height = window.innerHeight;

  const force = d3.layout
    .force()
    .nodes(d3.values(nodes))
    .links(links)
    .size([width, height])
    .linkDistance(400)
    .charge(-2000)
    .on('tick', tick)
    .start();

  const svg = d3
    .select('svg')
    .attr('width', width)
    .attr('height', height);

  // Per-type markers, as they don't inherit styles.
  svg
    .append('defs')
    .selectAll('marker')
    .data(force.links())
    .enter()
    .append('marker')
    .attr('id', function(d) {
      return d.weight;
    })
    .attr('viewBox', '0 -5 10 10')
    .style('stroke', function(d) {
      if (d.weightt > 5) {
        return 'none';
      } else {
        return 'none';
      }
    }) // colour the stroke
    .style('fill', function(d) {
      return d.weight === 1
        ? 'rgb(' + 109 + ',' + 236 + ',' + 252 + ',' + 0.9 + ')'
        : d.weight === 2
        ? 'rgb(' + 109 + ',' + 226 + ',' + 252 + ',' + 0.9 + ')'
        : d.weight === 3
        ? 'rgb(' + 109 + ',' + 219 + ',' + 252 + ',' + 0.9 + ')'
        : d.weight === 4
        ? 'rgb(' + 109 + ',' + 200 + ',' + 255 + ',' + 0.9 + ')'
        : d.weight === 5
        ? 'rgb(' + 109 + ',' + 183 + ',' + 252 + ',' + 0.9 + ')'
        : d.weight === 6
        ? 'rgb(' + 109 + ',' + 166 + ',' + 252 + ',' + 0.9 + ')'
        : d.weight === 7
        ? 'rgb(' + 109 + ',' + 140 + ',' + 252 + ',' + 0.9 + ')'
        : d.weight === 8
        ? 'rgb(' + 109 + ',' + 113 + ',' + 252 + ',' + 0.9 + ')'
        : d.weight === 9
        ? 'rgb(' + 128 + ',' + 109 + ',' + 252 + ',' + 0.9 + ')'
        : d.weight === 10
        ? 'rgb(' + 123 + ',' + 76 + ',' + 252 + ',' + 0.9 + ')'
        : 'rgb(' + 4 + ',' + 0 + ',' + 255 + ',' + 0.48 + ')';
    })

    .attr('refX', function(d) {
      return d.weight === 1
        ? 25
        : d.weight === 2
        ? 25
        : d.weight === 3
        ? 23
        : d.weight === 4
        ? 16
        : d.weight === 5
        ? 12
        : d.weight === 6
        ? 11
        : d.weight === 7
        ? 10
        : d.weight === 8
        ? 10
        : d.weight === 9
        ? 9
        : d.weight === 10
        ? 9
        : 20;
    })
    .attr('refY', function(d) {
      return d.weight === 1
        ? -0.2
        : d.weight === 2
        ? -0.5
        : d.weight === 3
        ? -0.2
        : d.weight === 4
        ? 0.6
        : d.weight === 5
        ? 0.5
        : d.weight === 6
        ? 0.4
        : d.weight === 7
        ? -0.3
        : d.weight === 8
        ? 0.2
        : d.weight === 9
        ? 0
        : d.weight === 10
        ? 0
        : 1;
    })
    .attr('markerWidth', function(d) {
      return d.weight === 1
        ? 7
        : d.weight === 2
        ? 3.5
        : d.weight === 3
        ? 2.4
        : d.weight === 4
        ? 2.5
        : d.weight === 5
        ? 2.6
        : d.weight === 6
        ? 2.7
        : d.weight === 7
        ? 2.6
        : d.weight === 8
        ? 2.3
        : d.weight === 9
        ? 2
        : d.weight === 10
        ? 2.2
        : 20;
    })
    .attr('markerHeight', function(d) {
      return d.weight === 1
        ? 7
        : d.weight === 2
        ? 3.5
        : d.weight === 3
        ? 2.4
        : d.weight === 4
        ? 2.5
        : d.weight === 5
        ? 2.6
        : d.weight === 6
        ? 2.7
        : d.weight === 7
        ? 2.6
        : d.weight === 8
        ? 2.3
        : d.weight === 9
        ? 2
        : d.weight === 10
        ? 2.2
        : 20;
    })
    .attr('orient', 'auto')
    .append('path')
    .attr('d', 'M0,-5L10,0L0,5');

  var path = svg
    .append('g')
    .selectAll('path')
    .data(force.links())
    .enter()
    .append('path')
    .attr('class', function(d) {
      return 'link ' + d.weight;
    })
    // .attr("stroke-width", 1)
    .attr('marker-end', function(d) {
      return 'url(#' + d.weight + ')';
    })

    .attr('source', function(d) {
      return d.source.name;
    })
    .attr('target', function(d) {
      return d.target.name;
    });

  var circle = svg
    .append('g')
    .selectAll('circle')
    .data(force.nodes())
    .enter()
    .append('circle')
    // .attr("r", 6)
    .data(force.links())
    .attr('r', function(d) {
      return 10;
    })
    .attr('stroke', function(d) {
      return getColor(d.weight);
    })
    .attr('fill', function(d) {
      return d.weight === 1
        ? 'rgb(' + 109 + ',' + 236 + ',' + 252 + ',' + 1 + ')'
        : d.weight === 2
        ? 'rgb(' + 109 + ',' + 226 + ',' + 252 + ',' + 1 + ')'
        : d.weight === 3
        ? 'rgb(' + 109 + ',' + 219 + ',' + 252 + ',' + 1 + ')'
        : d.weight === 4
        ? 'rgb(' + 109 + ',' + 200 + ',' + 255 + ',' + 1 + ')'
        : d.weight === 5
        ? 'rgb(' + 109 + ',' + 183 + ',' + 252 + ',' + 1 + ')'
        : d.weight === 6
        ? 'rgb(' + 109 + ',' + 166 + ',' + 252 + ',' + 1 + ')'
        : d.weight === 7
        ? 'rgb(' + 109 + ',' + 140 + ',' + 252 + ',' + 1 + ')'
        : d.weight === 8
        ? 'rgb(' + 109 + ',' + 113 + ',' + 252 + ',' + 1 + ')'
        : d.weight === 9
        ? 'rgb(' + 128 + ',' + 109 + ',' + 252 + ',' + 1 + ')'
        : d.weight === 10
        ? 'rgb(' + 123 + ',' + 76 + ',' + 252 + ',' + 1 + ')'
        : 'rgb(' + 4 + ',' + 0 + ',' + 255 + ',' + 0.48 + ')';
    })
    .data(force.nodes())
    .call(force.drag);

  var text = svg
    .append('g')
    .selectAll('text')
    .data(force.nodes())
    .enter()
    .append('text')
    .attr('x', 15)
    .attr('y', '.31em')
    .text(function(d) {
      return d.name;
    });

  // Use elliptical arc path segments to doubly-encode directionality.
  function tick() {
    path
      .attr('stroke', function(d) {
        return getColor(d.weight);
      })
      .attr('stroke-width', function(d) {
        return d.weight;
      })
      .attr('d', linkArc);
    circle
      // .data(force.links())
      // .attr("r", function (d) {
      //   return d.weight
      // })
      .data(force.nodes())
      .attr('transform', transform);
    text.attr('transform', transform);
  }

  function linkArc(d) {
    var dx = d.target.x - d.source.x,
      dy = d.target.y - d.source.y,
      dr = Math.sqrt(dx * dx + dy * dy);
    return (
      'M' +
      d.source.x +
      ',' +
      d.source.y +
      'A' +
      dr +
      ',' +
      dr +
      ' 0 0,1 ' +
      d.target.x +
      ',' +
      d.target.y
    );
  }

  function getColor(number) {
    switch (number) {
      case 1:
        return 'rgb(' + 109 + ',' + 236 + ',' + 252 + ',' + 0.48 + ')';
        break;
      case 2:
        return 'rgb(' + 109 + ',' + 226 + ',' + 252 + ',' + 0.48 + ')';
        break;
      case 3:
        return 'rgb(' + 109 + ',' + 219 + ',' + 252 + ',' + 0.48 + ')';
        break;
      case 4:
        return 'rgb(' + 109 + ',' + 200 + ',' + 255 + ',' + 0.48 + ')';
        break;
      case 5:
        return 'rgb(' + 109 + ',' + 183 + ',' + 252 + ',' + 0.48 + ')';
        break;
      case 6:
        return 'rgb(' + 109 + ',' + 166 + ',' + 252 + ',' + 0.48 + ')';
        break;
      case 7:
        return 'rgb(' + 109 + ',' + 140 + ',' + 252 + ',' + 0.48 + ')';
        break;
      case 8:
        return 'rgb(' + 109 + ',' + 113 + ',' + 252 + ',' + 0.48 + ')';
        break;
      case 9:
        return 'rgb(' + 128 + ',' + 109 + ',' + 252 + ',' + 0.48 + ')';
        break;
      case 10:
        return 'rgb(' + 123 + ',' + 76 + ',' + 252 + ',' + 0.48 + ')';
        break;
      default:
        return '#0950D0';
        break;
    }
  }

  function transform(d) {
    return 'translate(' + d.x + ',' + d.y + ')';
  }
};
